#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  main.py
#
#  Copyright 2021 Alexis Sostersich <axsoster@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# import os
# import json
import time as time_lib
import datetime as dt
import requests
import click


def check(url, timeout=1) -> ():
    if not isinstance(url, str):
        return None
    try:
        r = requests.get(url, stream=True, allow_redirects=True,
                         timeout=timeout)
        return (dt.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "OK",
                str(r.status_code), url)
    except Exception as e:
        return (dt.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "ERROR",
                str(None), url, str(e))


def get_country_ip(ip):
    return (requests.get(f"https://ip-api.io/json/{ip}").json())['country_name']


@click.command()
@click.option("--sites", default=None, help="Site list file",
              type=click.File('r'))
@click.option("--out", default=None, help="Outfile name")
@click.option("--time", default=30, help="Interval time to check (seconds)")
@click.option("--timeout", default=1, help="Timeout time (seconds)")
def main(sites, out, time, timeout):
    """Simple program to check url sites."""
    if sites is None:
        print("No sites to check")
        return

    url_sites = [url.strip() for url in sites
                 if not url.strip().startswith('#')]

    while True:
        for url in url_sites:
            print(' '.join(check(url, timeout)))

        print(dt.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "Waiting ...")
        time_lib.sleep(time)


if __name__ == '__main__':
    main()
