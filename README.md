# SiteChecker

Sites checker

## Clone repository
```
git clone git@gitlab.com:asoster/SiteChecker.git
```

## Activate virtual environment and install dependencies
```
python -m venv venv && . venv/bin/activate && pip install -U pip

pip install -r requirements.txt

```

## Check codestyle
```
python -m flake8 ; python -m pycodestyle
```

## Generate site list file
Create a file named `site_list.txt` (or other name you like) with a list of
sites you wants to check
```
https://gitlab.com
https://github.com
```
Dont forget the protocol in URLs.


## Use the application
```
python main.py --sites site_list.txt
```

### Make an alias in bash
```
alias sitechecker='cd ~/path/SiteCheker && . venv/bin/activate && python main.py --sites sites.txt;deactivate && cd ~'
```

## App Options
```
  --sites FILENAME   Site list file
  --out TEXT         Outfile name
  --time INTEGER     Interval time to check (seconds)
  --timeout INTEGER  Timeout time (seconds)
  --help             Show this message and exit.
```
